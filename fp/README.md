# Fundamentals of Programming

- [Computer Systems](./c01-computers.pdf)
- [Algorithms](./c02-algorithm.pdf)
- [C Programming](./c03-c-language.pdf)
